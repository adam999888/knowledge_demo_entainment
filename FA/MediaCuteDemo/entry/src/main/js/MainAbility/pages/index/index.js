/*
 * Copyright (c) 2021 Shenzhen Kaihong Digital Industry Development Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import myffmpegdemo from '@ohos.myffmpegdemo';
import prompt from '@ohos.prompt';
import router from '@ohos.router';
import fileio from '@ohos.fileio';


var globalThis;
export default {
    data: {
        title: '',
        srcOne: '/1.mp4',
        srcTwo: '/2.mp4',
        srcThree: '/3.mp4',
        srcOutOne: '/1_out.mp4',
        srcOutTwo: '/2_out.mp4',
        srcOutThree: '/3_out.mp4',
        // 裁剪前源文件
        src: '',
        // 提供给播放的裁剪前源文件
        srcPlay: '',
        // 裁剪后文件
        srcOut: '',
        // 提供给播放的裁剪后文件
        srcOutPlay: '',
        startTime: 0,
        endTime: 0,
        path1: '/data/accounts/account_0/appdata/com.example.mediacutedemo/',
        path2: 'com.example.entry/com.example.entry.MainAbility',
        path: '',
        isCuteSuccess: false
    },
    onInit() {
        this.title = this.$t('strings.world');
        console.log('gyf onInit');

        this.path = this.path1 + this.path2;

        globalThis = this;
    },

    srcOne() {
        this.src = this.path + this.srcOne;
        this.srcOut = this.path + this.srcOutOne;

        this.srcPlay = 'file://' + this.src;
        this.srcOutPlay = 'file://' + this.srcOut;
        console.log('gyf srcOne fileio open this.srcOut = ' + this.srcOut);

        this.fd = fileio.openSync(this.srcOut, 0o2 | 0o100 | 0o1000, 0o666);
        fileio.closeSync(this.fd);
    },

    srcTwo() {
        this.src = this.path + this.srcTwo;
        this.srcOut = this.path + this.srcOutTwo;

        this.srcPlay = 'file://' + this.src;
        this.srcOutPlay = 'file://' + this.srcOut;
        console.log('gyf srcTwo');

        this.fd = fileio.openSync(this.srcOut, 0o2 | 0o100 | 0o1000, 0o666);
        fileio.closeSync(this.fd);
    },

    srcThree() {
        this.src = this.path + this.srcThree;
        this.srcOut = this.path + this.srcOutThree;

        this.srcPlay = 'file://' + this.src;
        this.srcOutPlay = 'file://' + this.srcOut;
        console.log('gyf srcThree');

        this.fd = fileio.openSync(this.srcOut, 0o2 | 0o100 | 0o1000, 0o666);
        fileio.closeSync(this.fd);
    },


    changeStartTime(e) {
        this.startTime = Number(e.text);
        console.log('gyf change start time is ' + this.startTime.toString());
    },

    changeEndTime(e) {
        this.endTime = Number(e.text);
        console.log('gyf change end time is ' + this.endTime.toString());
    },

    playSrc() {
        console.log('gyf playSrc1');
        router.push({
            url: 'pages/player/player',
            params: {
                path: globalThis.srcPlay
            }
        });
    },

    playSrcOut() {
        console.log('gyf playSrcOut');
        router.push({
            url: 'pages/player/player',
            params: {
                path: globalThis.srcOutPlay
            }
        });
    },

    cutevideo() {
        globalThis.isCuteSuccess = false;
        console.log('gyf cutevideo');
        myffmpegdemo.videoCute(this.src, this.startTime, this.endTime, this.srcOut,
            function (result) {
                console.log('gyf cutevideo callback result = ' + result);
                globalThis.showPrompt('videoCute finished!');
                if (0 === result) {
                    globalThis.isCuteSuccess = true;
                } else {
                    globalThis.isCuteSuccess = false;
                }
            }
        );
    },

    // 弹框
    showPrompt(msg) {
        prompt.showToast({
            message: msg,
            duration: 1000
        });
    },
};



