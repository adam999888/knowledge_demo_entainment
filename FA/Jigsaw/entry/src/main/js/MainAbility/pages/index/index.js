﻿/*
 * Copyright 2022 LookerSong
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import router from '@system.router'

export default {
    data: {
        title: "拼图小游戏",
        rule: "玩法介绍：参照左边的原图，在屏幕上下左右滑动，使右边的拼图恢复原状，觉得困难可以开启提示哦"
    },

    playgame(num) {
        router.replace({
            uri: "pages/jigsaw/jigsaw",
            params: {
                block: num,
            },
        })
    }
}
