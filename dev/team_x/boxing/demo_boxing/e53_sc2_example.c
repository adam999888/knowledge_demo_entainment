/*
 * Copyright (c) 2020 Nanjing Xiaoxiongpai Intelligent Technology Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <stdio.h>
#include <string.h>
#include <unistd.h>

#include "E53_SC2.h"
#include "cmsis_os2.h"
#include "ohos_init.h"

#define TASK_STACK_SIZE (1024 * 8)
#define TASK_PRIO 25
#define ACCEL_THRESHOLD 100
#define TASK_DELAY_1S 1000000
#define FLAGS_MSK1 0x88000011U
#define Boxing_ACC 2.5

E53SC2Data data;

typedef struct {
    short   Temperature;
    float   Accel[3];
} myCaculdata;

myCaculdata myCaldata;

osEventFlagsId_t g_eventFlagsId; // event flags id

int AccDataHandle(E53SC2Data *dat)
{
    if(dat->Accel[ACCEL_X_AXIS] < 32764)
    {
        myCaldata.Accel[ACCEL_X_AXIS] = dat->Accel[ACCEL_X_AXIS]/4096.0;
    }
    else
    {
        myCaldata.Accel[ACCEL_X_AXIS] = 1-(dat->Accel[ACCEL_X_AXIS]-49512)/4096.0;
    }

    if(dat->Accel[ACCEL_Y_AXIS] < 32764)
    {
        myCaldata.Accel[ACCEL_Y_AXIS] = dat->Accel[ACCEL_Y_AXIS]/4096.0;
    }
    else
    {
        myCaldata.Accel[ACCEL_Y_AXIS] = 1-(dat->Accel[ACCEL_Y_AXIS]-49512)/4096.0;
    }

    if(dat->Accel[ACCEL_Z_AXIS] < 32764)
    {
        myCaldata.Accel[ACCEL_Z_AXIS] = dat->Accel[ACCEL_Z_AXIS]/4096.0;
    }
    else
    {
        myCaldata.Accel[ACCEL_Z_AXIS] = 1-(dat->Accel[ACCEL_Z_AXIS]-49512)/4096.0;
    }

return 0;
}

static void ExampleTask(void)
{
    uint8_t ret;
   
    int X = 0, Y = 0, Z = 0;

    ret = E53SC2Init();
    if (ret != 0) {
        printf("E53_SC2 Init failed!\r\n");
        return;
    }
    while (1) {

        ret = E53SC2ReadData(&data);
        if (ret != 0) {
            printf("E53_SC2 Read Data!\r\n");
            return;
        }
        AccDataHandle(&data);

        if(myCaldata.Accel[ACCEL_X_AXIS] < 0)
        {
            myCaldata.Accel[ACCEL_X_AXIS] = myCaldata.Accel[ACCEL_X_AXIS] * -1.0; 
        }

        if(myCaldata.Accel[ACCEL_Y_AXIS] < 0)
        {
            myCaldata.Accel[ACCEL_Y_AXIS] = myCaldata.Accel[ACCEL_Y_AXIS] * -1.0; 
        }

        if(myCaldata.Accel[ACCEL_Z_AXIS] < 0)
        {
            myCaldata.Accel[ACCEL_Z_AXIS] = myCaldata.Accel[ACCEL_Z_AXIS] * -1.0; 
        }

        if( myCaldata.Accel[ACCEL_X_AXIS] > Boxing_ACC || myCaldata.Accel[ACCEL_Y_AXIS] > Boxing_ACC || myCaldata.Accel[ACCEL_Z_AXIS] > Boxing_ACC)
        {
            
            //触发事件
            printf("MPU set flg\r\n");
            osEventFlagsSet(g_eventFlagsId, FLAGS_MSK1);

        }

        usleep(10000);//10ms
    }
}

static void ExampleEntry(void)
{
    osThreadAttr_t attr;

    g_eventFlagsId = osEventFlagsNew(NULL);
    if (g_eventFlagsId == NULL) {
        printf("Falied to create EventFlags!\n");
    }

    attr.name = "ExampleTask";
    attr.attr_bits = 0U;
    attr.cb_mem = NULL;
    attr.cb_size = 0U;
    attr.stack_mem = NULL;
    attr.stack_size = TASK_STACK_SIZE;
    attr.priority = TASK_PRIO;

    if (osThreadNew((osThreadFunc_t)ExampleTask, NULL, &attr) == NULL) {
        printf("Failed to create ExampleTask!\n");
    }
}

APP_FEATURE_INIT(ExampleEntry);