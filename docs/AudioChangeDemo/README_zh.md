# 录音变声应用代码介绍

## 应用端

### 简介

AudioChangeDemo是装在手机端的应用，适用于OpenHarmony3.1_release版本，使用AudioCapturer提供的JS接口对音频进行采集，并进行变声处理；

### 样例效果
![](resources/display1.jpg)

![](resources/display2.jpg)

&ensp;&ensp;&ensp;&ensp;首先开发设置音频加速或者减速，如果没有设置，默认是正常录音的速度，设置完成以后点击开始录音，启动录音功能，点击录音结束后，停止录音。录音完成以后点击播放，会显示出音频播放的组件，对录制的声音进行播放。

### 代码结构

本demo包括entry模块
<br>![](resources/1.png)

### 代码介绍

#### 1.创建AudioCapturer的实例
![](resources/2.png)

#### 2.创建数据存储的wav文件，创建成功后协商wav文件头信息
![](resources/3.png)

#### 3.开始数据采集
![](resources/4.png)

#### 4.读取数据，写入文件
![](resources/5.png)

#### 5.变声原理
&ensp;&ensp;&ensp;&ensp;我们可以对原始音频进行重采样，重采样有上采样和下采样，分别是对原始音频进行插值和抽取，比如P/Q重采样，一般我们的处理是，先对原始音频进行插值，在相邻两点间插入P个采样点，全部插入结束后再每隔Q个采样点进行采样，这样得到的音频语速和音调都是原来的Q/P倍。
<br>&ensp;&ensp;&ensp;&ensp;代码实现如下:
<br> ![](resources/6.png)

### 安装部署

##### 1.代码编译运行步骤

1）下载此项目，[链接](https://gitee.com/openharmony-sig/knowledge_demo_entainment/tree/master/FA/AudioChangeDemo)。

2）开发环境搭建，开发工具：DevEco Studio 3.0 Beta3 。

3）导入OpenHarmony工程：DevEco Studio 点击File -> Open 导入本样例的代码工程AudioChangeDemo。

4）OpenHarmony应用运行在真机设备上，需要进行签名。



