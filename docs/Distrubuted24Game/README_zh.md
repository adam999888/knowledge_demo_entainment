# 益智24点游戏应用代码介绍

### 简介

益智24点游戏是装在OpenHarmony系统中的应用，在两台设备间通过分布式数据共享实现数据间的通信，两台设备可以进行24点益智小游戏对战，一方优先提交正确的算式为胜利方。

### 样例效果
![](resources/1.gif)

### 代码结构

本demo包括entry模块 </br>
![](resources/1.png)


### 安装部署

##### 1.代码编译运行步骤

1）下载此项目，[链接](https://gitee.com/openharmony-sig/knowledge_demo_entainment/tree/master/FA/Distrubuted24Game )。

2）开发环境搭建，开发工具：DevEco Studio 3.0 Beta1 。

3）导入OpenHarmony工程：DevEco Studio 点击File -> Open 导入本样例的代码工程Distrubuted24Game。

4）OpenHarmony应用运行在真机设备上，需要进行签名。

### 约束限制

1. 提前准好已实名认证的开发者联盟账号 
2. 两台设备需要提前做好组网环境，需要在同一局域网环境下，通过点击“测试分布式任务”进入配置页面进行配置，配置完成后退出配置页面，通过点击“24点益智游戏”，进入对战页面。

