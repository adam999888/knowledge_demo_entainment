# 语音识别应用代码介绍

### 简介
    
VoiceRecoDemo是装在OpenHarmony系统中的应用，样例可以使用在汽车4S店的展示厅，客户可以通过语音来控制显示具体车辆的内饰，外观，车灯，后备箱等。样例利用了OpenHarmony的底层音频采集功能，获取到录音数据，将数据通过科大讯飞的接口进行语音识别。这个样例在rk3566板子上运行，由于设备没有mic，先使用录制好的pcm文件进行模拟音频采集，进行语音识别。


### 样例效果

![](2.png)

### 代码结构

本demo包括entry模块
![](1.png)



### 安装部署

##### 1.代码编译运行步骤

1）下载此项目，[链接](https://gitee.com/openharmony-sig/knowledge_demo_entainment/tree/master/FA/VoiceRecoDemo)。

2）开发环境搭建，开发工具：DevEco Studio 3.0 Beta1。

3）导入OpenHarmony工程：DevEco Studio 点击File -> Open 导入本样例的代码工程VoiceRecoDemo。

4）OpenHarmony应用运行在真机设备上，需要进行签名。
### 约束限制

1. 提前准好已实名认证的开发者联盟账号
2. 系统需要支持WebSocket的api，如果不支持需要自己添加对应的napi接口。
3. 如果设备没有mic，模拟使用audio目录下的pcm文件，将这些文件放置到设备的/data/audio目录下，代码中模拟的内饰，外观，引擎和尾部是通过读取/data/audio对应的pcm文件来获取语音数据的

### RK3566刷机流程
因为使用编译出来的kernel屏幕不亮屏，所以目前使用rk官方提供的linux sdk编译出的kernel
###
刷机包获取路径：链接:https://pan.baidu.com/s/1GM9RrKQtWMYKhsid9MeVSQ  提取码:7932

#### 1.进入loader模式
  1).按住 recovery键（靠近耳机处）并保持
  2).短按reset 键
  3).直到进入loader模式，松开recovery键

![](pic/2.png)

注意：

a.	如果打开了vmware虚拟机，观察是否有usb选择的弹框，选择连接到主机
b.	如何判断已经进入loader模式，如果进入该模式，RKDevTool页面会有如下提示

![](pic/3.png)

#### 2.烧录镜像
  先烧写这个镜像ROC-RK3566-PC-UBUNTU-GPT-20211015-0057.img，确保可以开机(如果出现进不去系统的情况，参考下文的‘常见异常’-2)

#### 3.再次进入loader模式，烧写自己编译出来的vender.img, userdata.img, system.img三个镜像(out/ohos-arm-release/packages/phone/images/目录下)
  a.	点‘1’，读取设备分区表，如’2’ (不用管报错)

  b.	修改’3‘ 处的三个分区，与右侧’2‘的三个分区信息保持一致

  c.	点’4‘选择镜像，’5‘进行烧录	(oem选择vendor.img, rootfs选择system.img)

![](pic/4.png)

#### 4.常见异常
  a.  ROC-RK3566-PC-UBUNTU-GPT-20211015-0057.img 无法开机的情况

    尝试如下连接中的‘方法二(原厂)’，进行操作
    https://wiki.t-firefly.com/zh_CN/ROC-RK3566-PC/03-upgrade_firmware_with_flash.html#maskrom-mo-shi-shao-xie-shi-bai
    链接中的步骤1中的MiniLoaderAll.bin文件，使用‘rk3566_自研板_环境与刷机.zip’里面的MiniLoaderAll.bin
    链接中的步骤6，使用ROC-RK3566-PC-UBUNTU-GPT-20211015-0057.img使用文件
