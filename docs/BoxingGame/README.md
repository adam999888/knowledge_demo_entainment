## OpenHarmony拳击游戏NAPI快速上手

### 样例简介

本样例是基于 OpenHarmony 3.2 beta1 版本，通过NAPI接口调用，实现拳击游戏的TCP网络连接和数据接收。

### 快速上手

#### 硬件准备

本样例采用润和RK3568开发板。

#### 环境准备

本样例是基于 OpenHarmony 3.2 beta1 版本开发的标准系统应用，使用的开发板是rk3568开发板，开发环境搭建也可以参照[ RK3568开发板上丝滑体验OpenHarmony标准系统](https://gitee.com/openharmony-sig/knowledge_demo_smart_home/blob/master/dev/docs/rk3568_quick_start/README.md)。

#### 工程准备

##### 下载OpenHarmony源码

```
repo init -u git@gitee.com:openharmony/manifest.git -b OpenHarmony-3.2-Beta1 --no-repo-verify
repo sync -c
repo forall -c 'git lfs pull'
```

##### 下载NAPI代码

NAPI代码存放于knowledge_demo_entainment仓库，先下载此仓库。

```
git clone https://gitee.com/openharmony-sig/knowledge_demo_entainment.git --depth=1
```

##### NAPI系统构建

###### NAPI代码拷贝

NAPI子系统代码位于knowledge_demo_entainment/dev/team_x/BoxingGame_NAPI/mysubsys文件夹中。

将mysubsys文件夹拷贝到OpenHarmony源码根目录。拷贝后的效果如下所示：

&nbsp;![](media/napi_tree.png)

###### 添加子系统

将子系统配置到\\\build\subsystem_config.json文件。

```
sudo vim build\subsystem_config.json
```

在该文件中插入如下内容（注意前后逗号，确保文件格式正确）：

```
"mysubsys": {
   "path": "mysubsys", 
   "name": "mysubsys"
  }
```

###### 添加组件

将组件添加到产品定义中，在vendor/hihope/rk3568/config.json文件中，subsystems下添加如下内容（注意前后逗号，确保文件格式正确）：

```
{
      "subsystem": "mysubsys",
      "components": [
        {
          "component": "boxinggame",
          "features": []
	      }
	    ]
},
```

添加后的效果如下所示：

&nbsp;![config_json](media/config_json.png)

#### 编译

进入源码根目录，执行如下命令进行版本编译：

```
./build.sh --product-name rk3568
```

编译成功后会显示“=====build rk3568 successful”。

####  烧录/安装

烧录方式参照[RK3568开发板上丝滑体验OpenHarmony标准系统](https://gitee.com/openharmony-sig/knowledge_demo_smart_home/blob/master/dev/docs/rk3568_quick_start/README.md#windows主机烧写固件)。

### NAPI接口定义

#### 应用端导入NAPI

```
import boxinggame_napi from '@ohos.boxinggame_napi'
```

#### NAPI接口调用

+ 应用端调用initServer初始化TCP服务端，初始化完成后设备可以通过TCP进行连接。

```
// 初始化TCP服务端
static napi_value initServer(napi_env env, napi_callback_info info)
```

+ 应用端调用recvMsg，返回值为左手挥拳或者右手挥拳的消息。

```
// 获取拳击游戏左右手挥拳消息
static napi_value recvMsg(napi_env env, napi_callback_info info)
```



### 参考资料

[RK3568开发板上丝滑体验OpenHarmony标准系统](https://gitee.com/openharmony-sig/knowledge_demo_smart_home/blob/master/dev/docs/rk3568_quick_start/README.md#windows主机烧写固件)

[深入浅出 OpenHarmony NAPI](https://gitee.com/javen678/hello-ohos-napi/tree/master/doc)

[标准系统编译构建指导](https://gitee.com/openharmony/docs/blob/OpenHarmony-v3.1-Beta/zh-cn/device-dev/subsystems/subsys-build-standard-large.md)