# 即时通讯《果聊》

## 果聊简介

- 即时通讯软件
- NutChat
今天打造的这一款即时通讯软件，可以应用在鸿蒙系统。

## 运行效果

在开始之前大家可以先预览一下我完成之后的效果。



是不是很炫酷呢？

## 工程版本

- 系统版本/API版本：OpenHarmony SDK API 8
- IDE版本：DevEco Studio 3.0 Beta4

##  快速上手

#### 准备硬件环境

- [获取OpenHarmony系统版本](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/get-code/sourcecode-acquire.md#获取方式3从镜像站点获取)：标准系统解决方案（二进制）

- [搭建标准系统环境](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-standard-env-setup.md)
- [完成DevEco Device Tool的安装](https://gitee.com/openharmony/docs/blob/master/zh-cn/device-dev/quick-start/quickstart-standard-env-setup.md)
- [完成Dayu200开发板的烧录](https://gitee.com/hihope_iot/docs/tree/master/HiHope_DAYU200/%E7%83%A7%E5%86%99%E5%B7%A5%E5%85%B7%E5%8F%8A%E6%8C%87%E5%8D%97)

#### 准备开发环境

- 安装最新版[DevEco Studio](https://gitee.com/link?target=https%3A%2F%2Fdeveloper.harmonyos.com%2Fcn%2Fdevelop%2Fdeveco-studio%23download_beta_openharmony)。
- 请参考[配置OpenHarmony SDK](https://gitee.com/openharmony/docs/blob/update_master_0323/zh-cn/application-dev/quick-start/configuring-openharmony-sdk.md)，完成**DevEco Studio**的安装和开发环境配置。
- 开发环境配置完成后，请参考[使用工程向导](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/start-with-ets.md) 创建工程（模板选择“Empty Ability”），选择eTS语言开发。
- 工程创建完成后，选择使用[真机进行调测](https://gitee.com/openharmony/docs/blob/master/zh-cn/application-dev/quick-start/start-with-ets.md#使用真机运行应用) 。

#### 准备工程

##### 工程下载

```
git clone https://gitee.com/openharmony-sig/knowledge_demo_entainment.git 
```

##### 工程导入

- DevEco Studio导入本工程;

  打开DevEco Studio,点击File->Open->下载路径/FA/GuoChat

#### 编译

- 点击**File > Project Structure** > **Project > Signing Configs**界面勾选“**Automatically generate signing**”，等待自动签名完成即可，点击“**OK**”。如下图所示：

![image-20220713103159887](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/image-20220713103159887.png)

- 点击Build->Build Hap/APPs 编译，编译成功生成entry-debug-rich-signed.hap

#### 烧录/安装

- 识别到设备后点击，或使用默认快捷键Shift+F10（macOS为Control+R)运行应用。

![img](https://luckly007.oss-cn-beijing.aliyuncs.com/macimages/install.png)

- [安装应用](https://gitee.com/openharmony/docs/blob/update_master_0323/zh-cn/application-dev/quick-start/installing-openharmony-app.md) 如果IDE没有识别到设备就需要通过命令安装，如下

  打开**OpenHarmony SDK路径 \toolchains** 文件夹下，执行如下hdc_std命令，其中**path**为hap包所在绝对路径。

  ```
  hdc_std install -r path\entry-debug-standard-ark-signed.hap
  ```

## 已实现的功能

### **1. 聊天界面**

- 消息列表
- 聊天窗口

### **2. 联系人界面**

- 好友列表
- 好友资料

### 3. 发现界面

- 圈子
- 会员中心

### **4. 我界面**

- 个人信息（使用设置类UI模板）



### **5. 聊天界面**

- 聊天输入框

- 聊天键盘

1. 更多键盘
