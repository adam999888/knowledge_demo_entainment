# [OpenHarmony 分布式音乐播放器](../../FA/DistrubutedMusicPlayer)

## 一、简介
#### 1.样例效果
[本Demo](../../FA/DistrubutedMusicPlayer)是基于OpenHarmony 3.1 Beta，使用ETS语言编写的应用。该样例使用eTS编写，可以播放多首wav格式的歌曲，并可使用分布式调度特性，进行跨设备流转。

![show](media/music.jpg)

HH-SCDAYU200

![3568](media/music_3568.gif)

Hi3516d

![show](media/show.gif)

#### 2.涉及OpenHarmony技术特性
- eTS-UI
- 音频
- 分布式调度

####  3.支持OpenHarmony版本
OpenHarmony 3.1 Beta。

#### 4.支持开发板
- 润和HiSpark Taurus AI Camera(Hi3516d)开发板套件（OpenHarmony 3.1 Beta）
- 润和大禹系列HH-SCDAYU200开发板套件（OpenHarmony 3.1 Beta）

## 二、快速上手
#### 1.标准设备环境准备

润和HiSpark Taurus AI Camera(Hi3516d)开发板套件:

- [Hi3516DV300开发板标准设备HelloWorld](../../../docs/hi3516_dv300_helloworld/README.md)，参考环境准备、编译和烧录章节

润和大禹系列HH-SCDAYU200开发套件：

- [开发板上新 | RK3568开发板上丝滑体验OpenHarmony标准系统](https://gitee.com/openharmony-sig/knowledge_demo_smart_home/tree/master/dev/docs/rk3568_quick_start)

#### 2.应用编译环境准备
- 下载DevEco Studio  [下载地址](https://developer.harmonyos.com/cn/develop/deveco-studio#download_beta)；
- 配置SDK，参考 [配置OpenHarmony-SDK](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.1-Beta/zh-cn/application-dev/quick-start/configuring-openharmony-sdk.md)
- DevEco Studio 点击File -> Open 导入本下面的代码工程DistrubutedMusicPlayer

润和大禹系列HH-SCDAYU200开发套件：

- [开发板上新 | RK3568开发板上丝滑体验OpenHarmony标准系统](https://gitee.com/openharmony-sig/knowledge_demo_smart_home/tree/master/dev/docs/rk3568_quick_start)

#### 3.项目下载和导入
1）git下载

```
git clone https://gitee.com/openharmony-sig/knowledge_demo_temp.git
```

2）项目导入

打开DevEco Studio,点击File->Open->下载路径/FA/Entertainment/DistrubutedMusicPlayer


#### 4.安装应用
- [配置应用签名信息](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.1-Beta/zh-cn/application-dev/quick-start/configuring-openharmony-app-signature.md)

- 安装应用

  打开**OpenHarmony SDK路径 \toolchains** 文件夹下，执行如下hdc_std命令，其中**path**为hap包所在绝对路径。

  ```text
  hdc_std install -r path\entry-debug-standard-ark-signed.hap
  ```

**PS** 分布式流转流转时，需要多个开发板，连接同一个wifi或使用网线连接


##  三、关键代码解读
#### 1.目录结构
```
.
├── entry\src\main\ets
│   ├── Common 
│   	├──components
│   		├──DeviceListDialog.ets //设备列表自定义弹窗组件
│   		├──MusicListDialog.ets //音乐列表自定义弹窗组件
│   	├──CommonLog.ets // 日志管理类
│   	├──PlayerManager.ets //音乐播放管理类
│   	├──RemoteDeviceManager.ets //设备管理类，用于发现设备，认证设备
│   ├── MainAbility  
│   	├──pages
│   		├──index.ets //播放列表主页面
│   	├──app.ets //ets应用程序主入口
```

#### 2.日志查看方法
```
hdc_std shell
hilog | grep MyOpenHarmonyPlayer  
```

#### 3.关键代码
- UI界面，设备流转：index.ets
- 音乐播放：PlayerManager.ets
- 设备管理：RemoteDeviceManager.ets

##  四、参考链接
- [OpenHarmony JS开发参考](https://gitee.com/openharmony/docs/tree/OpenHarmony-3.0-LTS/zh-cn/application-dev/js-reference)
- [OpenHarmony FeatureAbility模块](https://gitee.com/openharmony/docs/blob/OpenHarmony-3.0-LTS/zh-cn/application-dev/js-reference/apis/js-apis-featureAbility.md)
- [OpenHarmony js 分布式播放器Sample](https://gitee.com/openharmony/app_samples/tree/master/ability/JsDistributedMusicPlayer)
- [开发板上新 | RK3568开发板上丝滑体验OpenHarmony标准系统](https://gitee.com/openharmony-sig/knowledge_demo_smart_home/tree/master/dev/docs/rk3568_quick_start)

